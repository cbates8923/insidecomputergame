extends StaticBody2D


@onready var player = get_parent().get_node("PlayerBody")


func _physics_process(delta):
	if $RayCasts/Down.is_colliding() and "PlayerBody" in $RayCasts/Down.get_collider().name:
		$CollisionShape2D.disabled = true
	elif $RayCasts/Up.is_colliding() and "PlayerBody" in $RayCasts/Up.get_collider().name:
		$CollisionShape2D.disabled = false
