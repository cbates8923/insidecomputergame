extends AnimatedSprite2D

@export var speed = 8
var alternative_point = null
var arrived_at_alt_pt = false 
var removing = false

func _ready():
	add_to_group("packets")
	
	modulate = Color(1, 1, 1, 0)
	
	animation = ["packet1", "packet2", "packet3"][(randi() % 4) - 1]

func _process(delta):
	var player = get_parent().get_parent().get_node_or_null("PlayerBody")
	
	if modulate.a < 1:
		modulate.a += 0.01
		
	if modulate.a > 0 and removing:
		modulate.a -= 0.02
	
	if not player:
		player = get_parent().get_node_or_null("PlayerBody")
		
		if not player:
			return
	
	var offset = player.position + player.up_direction * 128 - position
	
	if alternative_point:
		offset = alternative_point - position
		
		if position.distance_to(alternative_point) < 0.1:
			arrived_at_alt_pt = true
	else:
		arrived_at_alt_pt = false
	
	position += offset * delta * 4
	rotation += (atan2(player.up_direction.y, player.up_direction.x) + 3.1415 / 2 - rotation) * delta * 8
