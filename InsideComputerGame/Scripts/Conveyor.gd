extends Node2D


@export var speed = 1200
@export var dir = 1

# Called when the node enters the scene tree for the first time.
func _process(_delta):
	$AnimatedSprite2D.animation = "Moving"
	$AnimatedSprite2D.play()
