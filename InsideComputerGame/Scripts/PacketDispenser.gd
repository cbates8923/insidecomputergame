extends CharacterBody2D

# Load packet
@onready var packet = preload("res://Packet.tscn")

# Variable for if this is a packet receiver
@export var is_packet_receiver = false

# Progress for animation of coming out of dispenser
var packet_spawn_progress = 0

# Variable that stores the object of the packet to animate
var animated_packet = null

# If dispenser has spawned a packet yet.
var spawned_packet = false

# Variable for if we are inserting the packet into the machine
var inserting_packet = false 

# If we are waiting for packet to move into place before animating it going into
# the slot, set this variable to true
var waiting_for_pack_insrt = false

func _ready():
	add_to_group("unwalkable")
	
	# Add to group receiver/dispensers so we can have new packets once ours
	# has been dispensed.
	if not is_packet_receiver:
		add_to_group("packet_dispensers")
	else:
		add_to_group("packet_receivers")

func _physics_process(delta):
	# If everything is correct to spawn a packet, instantiate one and add it as
	# a child of the parent, also storing it in the animated packet variable. If
	# we are a receiver, set animated_packet to the most recent addition to the 
	# packets group, and let it animate to that position.
	if $PlayerPushPiston.state == "up" and Input.is_action_just_pressed("interact") and not spawned_packet:
		if not is_packet_receiver:
			var instanced_packet = packet.instantiate()
			instanced_packet.position = position + $PacketSpawnPoint.position
			animated_packet = instanced_packet
			spawned_packet = true
			get_parent().add_child(instanced_packet)
			
			for packet_receiver in get_tree().get_nodes_in_group("packet_receivers"):
				packet_receiver.spawned_packet = false
		elif len(get_tree().get_nodes_in_group("packets")) > 0:
			animated_packet = get_tree().get_nodes_in_group("packets")[-1]
			animated_packet.alternative_point = position + $PacketSpawnPoint.position - Vector2(0, 128)
			waiting_for_pack_insrt = true 
				
			for packet_dispenser in get_tree().get_nodes_in_group("packet_dispensers"):
				packet_dispenser.spawned_packet = false
	
	if waiting_for_pack_insrt:
		if animated_packet.arrived_at_alt_pt:
			waiting_for_pack_insrt = false 
			animated_packet.removing = true
		
	# If packet should be animated, handle animations.
	if animated_packet and not waiting_for_pack_insrt:
		
		# If we dispense packets, animate the packet moving out of the slot,
		# otherwise animate it going back into the slot, then delete the
		# packet.
		if not is_packet_receiver:
			animated_packet.position = position + $PacketSpawnPoint.position - Vector2(0, packet_spawn_progress)
		elif animated_packet.arrived_at_alt_pt or inserting_packet:
			inserting_packet = true
			animated_packet.alternative_point = null
			animated_packet.position = position + $PacketSpawnPoint.position - Vector2(0, 128) + Vector2(0, packet_spawn_progress)
		
		packet_spawn_progress += 1
		
		if packet_spawn_progress > 128:
			inserting_packet = false
			packet_spawn_progress = 0
			
			# Delete packet once is in slot, not necesary.
			if is_packet_receiver:
				animated_packet.queue_free()
				
			animated_packet = null
	

func _on_player_detection_area_body_entered(body):
	if "PlayerBody" in body.name:
		$PlayerPushPiston.state = "up"


func _on_player_detection_area_body_exited(body):
	if "PlayerBody" in body.name:
		$PlayerPushPiston.state = "down"
