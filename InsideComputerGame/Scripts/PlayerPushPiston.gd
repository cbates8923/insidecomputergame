extends CharacterBody2D

# Distance to push the piston, in pixels.
@export var push_distance = 24

# Should we push on the x axis instead?
@export var push_x_axis = false

# Direction, -1, 1
@export var direction = 1

var state = "down"
var offset = 0
var finished = true

func _physics_process(delta):
	# Check if we need to lower the piston. if so or otherwise update the 
	# finished variable accordingly.
	if state == "down" and offset > 0:
		finished = false
		offset -= 1
		
		if not push_x_axis:
			position.y += direction
		else:
			position.x += direction
	elif state == "down":
		finished = true
		
	# Save thing here, but we aren't lowering.
	if state == "up" and offset < push_distance:
		finished = false
		offset += 1
		
		if not push_x_axis:
			position.y -= direction
		else:
			position.x -= direction
	elif state == "up":
		finished = true
