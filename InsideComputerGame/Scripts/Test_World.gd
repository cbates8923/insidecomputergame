extends Node2D


@export var paralax_scaling = Vector2.ZERO


func update_paralax(velocity):
	$ManualParalax.position += paralax_scaling * velocity
