extends CharacterBody2D


const SPEED = 300.0

var state = "floor"

var gravity = ProjectSettings.get_setting("physics/2d/default_gravity_vector")
@onready var rays = [$RayCasts/Right, $RayCasts/Left, $RayCasts/Up, $RayCasts/Down]
var ray_cols_before = [false, false, false, false]

var movement_delta = Vector2.ZERO
var previous_position = position

func _ready():
	for ray in rays:
		ray.add_exception(self)


func set_all_ray_rots():
	$RayCasts.rotation = -rotation


func set_sprite_hitbox_rot(degrees):
	set_all_ray_rots()
	$AnimatedSprite2D.rotation_degrees = degrees
	$CollisionShape2D.rotation_degrees = degrees
	$Camera2D.rotation_degrees = degrees
	
	
func run_conveyor_physics(conveyor):
	if state == "floor":
		velocity.x = conveyor.speed * conveyor.dir
	if state == "up":
		velocity.x = conveyor.speed * -conveyor.dir


func check_walkable(node):
	if node in get_tree().get_nodes_in_group("unwalkable"):
		return false
	return true


func _physics_process(delta):
	velocity += gravity
	
	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var direction = Input.get_axis("move_left", "move_right")
	if direction != 0:
		if state == "floor":
			velocity.x += 10 * direction
		if state == "up":
			velocity.x += 10 * -direction
		if state == "left":
			velocity.y += 10 * direction
		if state == "right":
			velocity.y += 10 * -direction
	elif state == "right" or state == "left":
		velocity.x = move_toward(velocity.x, 0, 1000)
		
	if direction < 0:
		$AnimatedSprite2D.animation = "Left"
		$AnimatedSprite2D.play()
	if direction > 0:
		$AnimatedSprite2D.animation = "Right"
		$AnimatedSprite2D.play()
	if direction == 0:
		$AnimatedSprite2D.animation = "Idle"
		$AnimatedSprite2D.play()
	
	# Set wall direction
	# ------------------
	
	# Check for racasts going in a direction, to see if we can go on a wall.
	if $RayCasts/Right.is_colliding() and Input.is_action_pressed("move_right") and state == "floor" and check_walkable($RayCasts/Right.get_collider()):
		velocity.y = -velocity.x
		state = "right"
	if $RayCasts/Down.is_colliding() and Input.is_action_pressed("move_right") and state == "left" and check_walkable($RayCasts/Down.get_collider()):
		velocity.x = velocity.y
		state = "floor"
	if $RayCasts/Left.is_colliding() and Input.is_action_pressed("move_right") and state == "up" and check_walkable($RayCasts/Left.get_collider()):
		velocity.y = -velocity.x
		state = "left"
	if $RayCasts/Up.is_colliding() and Input.is_action_pressed("move_right") and state == "right" and check_walkable($RayCasts/Up.get_collider()):
		velocity.x = velocity.y
		state = "up"
		
	# Same thing but for moving left.
	if $RayCasts/Left.is_colliding() and Input.is_action_pressed("move_left") and state == "floor" and check_walkable($RayCasts/Left.get_collider()):
		velocity.y = velocity.x
		state = "left"
	if $RayCasts/Up.is_colliding() and Input.is_action_pressed("move_left") and state == "left" and check_walkable($RayCasts/Up.get_collider()):
		velocity.x = -velocity.y
		state = "up"
	if $RayCasts/Right.is_colliding() and Input.is_action_pressed("move_left") and state == "up" and check_walkable($RayCasts/Right.get_collider()):
		velocity.y = velocity.x
		state = "right"
	if $RayCasts/Down.is_colliding() and Input.is_action_pressed("move_left") and state == "right" and check_walkable($RayCasts/Down.get_collider()):
		velocity.x = -velocity.y
		state = "floor"
		
	# Do stuff based on the state we are in. Relatively similar things for all states.
	if state == "floor":
		# Set sprite/hitbox rotation, but not raycasts.
		set_sprite_hitbox_rot(0)
		
		# Set the gravity so that we fall in the right direction.
		gravity = Vector2(0, ProjectSettings.get_setting("physics/2d/default_gravity"))
		
		# Up direction is used in collisions.
		up_direction = Vector2(0, -1)
		
		# Check if we are on a conveyor and if so, run physics for that.
		if $RayCasts/Down.is_colliding() and "Conveyor" in $RayCasts/Down.get_collider().name:
			run_conveyor_physics($RayCasts/Down.get_collider())
	if state == "right":
		set_sprite_hitbox_rot(-90)
		gravity = Vector2(ProjectSettings.get_setting("physics/2d/default_gravity"), 0)
		up_direction = Vector2(-1, 0)
		
		# Run horizontal gravity, because it's glitchy otherwise.
		if not $RayCasts/Right.is_colliding():
			velocity.x += gravity.x * 6
	if state == "up":
		set_sprite_hitbox_rot(180)
		gravity = Vector2(0, -ProjectSettings.get_setting("physics/2d/default_gravity"))
		up_direction = Vector2(0, 1)
		
		if $RayCasts/Up.is_colliding() and "Conveyor" in $RayCasts/Up.get_collider().name:
			run_conveyor_physics($RayCasts/Up.get_collider())
	if state == "left":
		set_sprite_hitbox_rot(90)
		gravity = Vector2(-ProjectSettings.get_setting("physics/2d/default_gravity"), 0)
		up_direction = Vector2(1, 0)
		
		if not $RayCasts/Left.is_colliding():
			velocity.x += gravity.x * 6
			
	# Code for invert gravity.
	if Input.is_action_just_pressed("invert"):
		if state == "floor":
			state = "up"
		elif state == "up":
			state = "floor"
		elif state == "right":
			state = "left"
		elif state == "left":
			state = "right"
	
	# ------------------
	
	# Call move and slide for collisions
	move_and_slide()
	
	# Update the paralax backgrounds based on the movement delta
	movement_delta = previous_position - position
	get_parent().update_paralax(movement_delta)
	
	previous_position = position
